package GenericLibraries;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class BaseClassConfiguration {
	
public static WebDriver driver;
	
	@Parameters({"browser"})
	@BeforeClass
	public void openBrowser(String browser) 
	{
			if (browser.equalsIgnoreCase("Firefox")) 
			{
				driver = new FirefoxDriver();
			}
			else if (browser.equalsIgnoreCase("chrome")) 
			{
				System.setProperty("webdriver.chrome.driver",
						"C:\\Users\\dibyaprakash.sahoo\\eclipse-workspace\\MyProjectTripAdviser\\Drivers\\chromedriver_win32\\chromedriver.exe");
				
				driver = new ChromeDriver();
			}
}
	
	

	
	
@AfterClass
public void closeBrowser() {
	driver.quit();
}


}
	
	
	


