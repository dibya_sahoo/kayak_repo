package GenericLibraries;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import TestScriptRepository.TestBookMyFlight;

public class AvailScreenShot implements ITestListener {

	// A method defined to take a screenshot at any time by calling this method
	public void onTestFailure(ITestResult result) {
		// since you need the driver in your screenshot method do this

		if (result.getStatus() == ITestResult.FAILURE) {
			try {
				EventFiringWebDriver efdriver = new EventFiringWebDriver(TestBookMyFlight.driver);
				File src = efdriver.getScreenshotAs(OutputType.FILE);
				String destinationfilePath = "C:\\Users\\dibyaprakash.sahoo\\eclipse-workspace\\Kayak2ndWeekFlightBookingTest\\Screenshots\\";
				// result.getName() will return name of test case so that screenshot name will
				// be same as test case name

				FileUtils.copyFile(src, new File(destinationfilePath + result.getName() + ".png"));
				System.out.println("Successfully captured a screenshot");
			}

			catch (Exception e) {
				System.out.println("Exception while taking screenshot " + e.getMessage());
			}

		}
	}

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub

	}

}
