package TestScriptRepository;



import java.awt.Robot;
import java.io.FileInputStream;
import java.util.Properties;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.thoughtworks.selenium.webdriven.commands.KeyEvent;

import GenericLibraries.BaseClassConfiguration;
import PageFactory.HomePage;

public class TestBookMyFlight extends BaseClassConfiguration {
	HomePage home;

	@Test
	public void VerifyBookingScenarios() throws Exception {
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		Properties prop = new Properties();
		FileInputStream stream = new FileInputStream("InputData.properties");
		prop.load(stream);

		driver.get(prop.getProperty("URL"));
		home = PageFactory.initElements(driver, HomePage.class);
		home.selectFromAirPort();
		home.selectTOairPort();
		HomePage.selectDate(driver, prop.getProperty("startDate"));
		HomePage.selectDate(driver, prop.getProperty("endDate"));
		driver.findElement(By.xpath("//div[contains(@class,' centre')]/button[contains(@id,'submit')]")).click();
		System.out.println("search button clicked");
		Thread.sleep(20000);
		
		String ParentWindowID = driver.getWindowHandle();
		System.out.println("parent:" + ParentWindowID);
		
		
		driver.switchTo().window(ParentWindowID);


		Robot r = new Robot();
		Thread.sleep(1000);
		r.keyPress(java.awt.event.KeyEvent.VK_ESCAPE);
		r.keyRelease(java.awt.event.KeyEvent.VK_ESCAPE);
		Thread.sleep(1000);
		
        String pagetitle= driver.getTitle();
        System.out.println(pagetitle);
		Assert.assertEquals(true,pagetitle.contains("MAA"));

	}

}
