package PageFactory;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {
	
	@FindBy(name="origin")
	WebElement Enter_FROMairport;                                     					//Path of From_AirportName 
	
	@FindBy(name="destination")
	WebElement Enter_TOairport;                                      					 //Path of To_Airport Name
	
	
	@FindBy(xpath="//div[contains(@id,'depart-input')]")               					// Path of  Journey Date
	
	WebElement Select_FromDate;
	
	
	@FindBy(xpath="//div[contains(@id,'return-input')]")                				// Path of  Return Date
	
	WebElement Select_TODate;
	
	
	@FindBy(xpath = "//div[contains(@class,' centre')]/button[contains(@id,'submit')]")	 //Path of  Search Button 
	WebElement SearchButton;


	public void selectFromAirPort() throws InterruptedException {
		Enter_FROMairport.clear();
		Enter_FROMairport.sendKeys("Chennai");
		Thread.sleep(1000);
        Enter_FROMairport.sendKeys(Keys.TAB);
		
	}
	
	public void selectTOairPort() throws InterruptedException {

		Enter_TOairport.clear();
		Enter_TOairport.sendKeys("pune");
		Thread.sleep(1000);

        Enter_TOairport.sendKeys(Keys.TAB);
		
	}
	
	
	////////////////////////////////////////////////
	
	public static void selectDate(WebDriver driver,String date) throws Exception
	{
	String[] dateArray=date.split("-");
	String day= dateArray[0];
	System.out.println("Day is:"+day);
	String month=dateArray[1];
	System.out.println("Month is:"+month);
	String year=dateArray[2];
	System.out.println("Year is:"+year);
	String monthAndyear=month+" "+year;
	System.out.println("Month+Year is:"+monthAndyear);
	findMonthAndYear(driver,monthAndyear);
	String monthAndday=month+" "+day;
	System.out.println("Month+Day is:"+monthAndday);
	Properties p= new Properties();
	FileInputStream fis= new FileInputStream("Months.Properties");
	p.load(fis);
	System.out.println("Properties file loaded succesfully");
	String YearAndMonth= year+ p.getProperty(month);
	System.out.println("Year And Month is:"+YearAndMonth);
	driver.findElement(By.xpath("//div[contains(@id,'"+YearAndMonth+"')]"
			              + "/div[@class='month']/div[@class='weeks']"
			              + "/div[@class='keel-grid weekGrid']"
			              + "/div[@aria-label='"+monthAndday+"']")).click();
	
	}


	public static void findMonthAndYear(WebDriver driver, String monthAndYear) throws InterruptedException
	{
	if(driver.findElement(By.xpath("//*[contains(text(),'" + monthAndYear + "')]")).isDisplayed())
	{
		System.out.println("Calender month and year Found");
	} 
	
		else
		{	
			driver.findElement(By.xpath("//div[@class ='displayWrapper']/div[@class ='default-view']/div[@class ='navItem nextMonth']")).click();
	    	Thread.sleep(400);

			findMonthAndYear(driver, monthAndYear);
		}
	    
	   
	
	

	}

}
